
const express = require ('express');
const hello = require ('../controllers/hello.js');
const get = require ('../controllers/get.js');
const fork = require ('../controllers/fork.js');
const write_file = require ('../controllers/write_file.js');
const make_pull_request = require ('../controllers/make_pull_request.js');


const router = express.Router();

router.get('/',hello.hello);
router.get('/get',get.get);
router.post('/fork',fork.fork);
router.post('/write_file',write_file.write_file);
router.post('/make_pull_request',make_pull_request.make_pull_request);

module.exports = router;