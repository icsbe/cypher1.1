const fetch = require('node-fetch');

exports.make_pull_request = async function   (req, res)  {
    //console.log(req.body);
    var results = await  
    fetch('https://api.bitbucket.org/2.0/repositories/'+req.body.destination_workspace_slash_repo
    +'/pullrequests',
      {method: "POST", body: 
      JSON.stringify(
        {
                "title": req.body.title,
                "source": {
                    "repository" : {
                        "full_name": req.body.source_workspace_slash_repo
                    },
                    "branch": {
                        "name": req.body.source_branch
                    }
                },
                        "destination": {
                    "branch": {
                        "name": req.body.destination_branch
                    }
                }
        }
        )
      ,
     
      headers: {   'Accept': 'application/json',
          'Content-Type': 'application/json' , 'Authorization': 
          'Bearer C2jcKvO3JmDqzMdLxvMvZtISMGhJjqPCQEyswOO79IO_C41AAamiMAyF6pCRcO7sWlCoRVghO8d3CIPMYTvJ34ZnBGjhGtqmo4SbasFHjT7TPA2-SB2Ihli4'}
      
      }
      )
     // .then(response =>response.json())
        .catch(error => console.error(error));
     res.json(results);
    };

