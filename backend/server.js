const mongo = require('mongodb');
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const router = require ('./routes/router.js');
const app = express();
const port = 8080; 

app.use(bodyParser.json())

var corsOption = {
	origin: true,
	methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
	credentials: true,
	exposedHeaders: ['x-auth-token']
  };
  app.use(cors(corsOption));

app.post('/login', bodyParser.json(), (req, res, next) =>{
    mongo.MongoClient.connect('mongodb://localhost:27017', function(err, db){
        if (err) throw err;
        db.db('cypher').collection('users').find({username:req.body.username, password:req.body.password}).toArray(function(err, response){
            if (err) throw err;
            db.close();
            if (response.length == 0) {
                res.sendStatus(404);
            } else {
                res.sendStatus(200);
            }
        })
    });
});

app.use('/api/',router)



// Run server
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});