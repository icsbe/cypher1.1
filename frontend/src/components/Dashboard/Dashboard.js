import React from 'react'
import Navbar from '../navbar/Navbar'
import {withRouter} from 'react-router-dom';

function Dashboard() {
    return (
        <div>
            <Navbar/>
        </div>
    )
}

export default withRouter(Dashboard);