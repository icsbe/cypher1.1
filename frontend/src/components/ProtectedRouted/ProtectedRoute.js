import React from 'react'
import {Route, Redirect} from 'react-router-dom'
import Auth from '../../services/Auth';

//isAuth check if you are authorized, component is the component you want to route to, rest are some extra props
export default function ProtectedRoute({ component:Component,  ...rest} ) {
    return (
        <Route {...rest} render={(props) => {
            if(Auth.isAuthenticated()){
                return <Component />;
            } else {
                return <Redirect to={{pathname: '/', state: {from: props.location}}}/>;
            }
        }} />
    )
}
