import "bootstrap/dist/css/bootstrap.min.css";
import "./Navbar.css";
import logo from "../../assets/images/cypherlogosmall.png";

import { HiOutlineLogout } from "react-icons/hi";
import { AiOutlinePullRequest } from "react-icons/ai";
import { MdOutlineSaveAlt } from "react-icons/md";
import React from "react";
import Auth from "../../services/Auth";
import { useHistory } from "react-router-dom";

export default function Navbar(props) {
  let history = useHistory();
  return (
    <div class="topnavbar">
      <div class="info">
        <img src={logo} alt="logo" />
        <h2>Cypher</h2>
      </div>
      <div class="actions">
        <span id="saved"></span>
        <button className="btn btn-primary" onClick={Save}>
          <MdOutlineSaveAlt size={21} /> &nbsp; Save
        </button>
        <button className="btn btn-outline-light" onClick={PullRequest}>
          <AiOutlinePullRequest size={21} /> &nbsp; Pull Request
        </button>
        <button className="btn btn-outline-light" onClick={Logout}>
          <HiOutlineLogout size={25} />{" "}
        </button>
      </div>
    </div>
  );

  //Function to logout
  function Logout() {
    Auth.logout();
    history.push('/')
  }
}

//Function to save new changes of translations to copy of Bitbucket repository
function Save() {}

//Function to make a pull request from copy of Bitbucket repository to real repository
function PullRequest() {}
