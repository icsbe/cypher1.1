import 'bootstrap/dist/css/bootstrap.min.css';
import { React } from 'react';
import './Login.css';
import * as Yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useHistory } from "react-router-dom";
import Auth from '../../services/Auth';

function Login() {
  let history = useHistory();
  
  const validationSchema = Yup.object({
    username: Yup.string().required('Username is required'),
    password: Yup.string()
      .required('Password is required')
      .min(6, 'Password must be at least 6 characters')
      .max(20, 'Password must not exceed 20 characters')
  });

  const {register, handleSubmit, reset, formState: {errors}} = useForm({
    resolver: yupResolver(validationSchema)
  });

  const OnSubmit = data => {
    console.log(JSON.stringify(data));
    console.log("test");
    reset();

    Auth.login();
    //navigate to dashboard
    history.push("/dashboard");
  }

  return (
    <div className="d-flex flex-row justify-content-center align-items-center vh-100 vw-100">
      <div className="w-50 align-items-center p-5">
          <h3><b>Login</b></h3>
          <small className="text-muted">Login to Cyper to manage your project translations</small>
          <br /><br />
          <form onSubmit={handleSubmit(OnSubmit)}>
            <div className="form-group mt-5">
              <label for="usernameInput"><b>Username</b></label>
              <input type="text" {...register('username')} className={`form-control rounded-pill mt-3 ${errors.username ? 'is-invalid' : ''}`} id="usernameInput" placeholder="Username" />
              <div className="invalid-feedback">{errors.username?.message}</div>
            </div>
            <div className="form-group mt-5">
              <label for="passwordInput"><b>Password</b></label>
              <input type="password" {...register('password')} className={`form-control rounded-pill mt-3 ${errors.password ? 'is-invalid' : ''}`} id="passwordInput" placeholder="Password" />
              <div className="invalid-feedback">{errors.password?.message}</div>
            </div>
            <button type="submit" className="btn btn-primary mt-5 mb-2 rounded-pill w-100">Login</button>
          </form>
        </div>
    </div>
  );

  
}

export default Login;
