class Auth{
    constructor(){
    }

    login(){
        this.authenticated = true
        sessionStorage.setItem('authenticated', true)
    }

    logout(){
        sessionStorage.setItem('authenticated', false)
    }

    isAuthenticated() {
        return sessionStorage.getItem('authenticated')
    }
}

export default new Auth()