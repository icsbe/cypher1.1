import './App.css';
import Login from './components/login/Login';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dashboard from './components/Dashboard/Dashboard';
import ProtectedRoute from './components/ProtectedRouted/ProtectedRoute';

function App() {
  return (
    <div className="App">
      <Router>
        <div id="pg-content" className='content'>
          <Switch>
            <Route path="/" exact ><Login /></Route>
          </Switch>
        </div>
        <ProtectedRoute path="/dashboard" component={Dashboard}/>    
      </Router>
      
    </div>
  )
}

export default App;
